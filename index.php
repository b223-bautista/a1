<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Selection Control Structures and Array Manipulation - Activity</title>
</head>
<body>
	<h2>create a function named printDivisibleOfFive that will perform the following:</h2>
	<pre><?php printDivisibleOfFive(); ?></pre>

	<h2>Accept a name of the student and add it to the "students" array</h2>
	<?php array_push($students, 'John Smith'); ?>
	<pre><?php print_r($students); ?></pre>

	<h2>Count the number of names in the "students" array.</h2>
	<pre><?php echo count($students)?></pre>

	<h2> Add another student then print the array and its new count.</h2>
	<?php array_push($students, 'Jane Smith'); ?>
	<pre><?php print_r($students); ?></pre>

	<h2>Finally, remove the firs student and print the array and its count.</h2>
	<?php array_shift($students); ?>
	<pre><?php print_r($students); ?></pre>
</body>
</html>